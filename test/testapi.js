const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var expect = chai.expect;

describe('First test',
  function(){
    it('Test that DuckDuckgo works', function(done){
        chai.request('http://www.duckduckgo.com/r/t')
          .get('/').end(function(err, res) {
            console.log("Request has finished");
            if (err != null)
              console.log(err);
            res.should.have.status(200);
            done();
          }
        )
      }
    )
  }
);

describe('Test de API Usuarios',
  function(){
    it('Test that user API works', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello').end(function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            if (err != null)
              console.log(err);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde API TechU");
            done();
          }
        )
      }
    )
    it('Prueba q la API devuelve una lista de usarios correctos', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users').end(function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            //console.log(err);
            res.should.have.status(200);
            res.body.data.should.be.a('array');

            for (user of res.body.data){
              user.should.have.property("email");
              user.should.have.property("first_name");
            }
            done();
          }
        )
      }
    )
  }
);

describe('Test de API Accounts',
  function(){
    it('Test that Account API works', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v2/apuntes/PL30 7555 5903 6997 1523 6369 7176').end(function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            if (err != null)
              console.log(err);
            res.should.have.status(200);
            //console.log(res.body);
            res.body.should.be.a('array');
            expect(res.body.length).to.be.above(3);
            done();
          }
        )
      }
    )
    it('Prueba q la API devuelve una lista de cuentas del usuario 3', function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v2/accounts/3').end(function(err, res) {
            //console.log(res);
            console.log("Request has finished");
            if (err != null)
              console.log(err);
            res.should.have.status(200);
            //console.log(res.body);
            res.body.should.be.a('array');
            expect(res.body.length).to.be.above(2);

            for (account of res.body){
              account.should.have.property("iban");
              account.should.have.property("balance");
              account.should.have.property("balanceUSD");
            }
            done();
          }
        )
      }
    )
  }
);
