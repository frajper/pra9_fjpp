# Imagen raíz
FROM node

# Directorio raíz
WORKDIR /apitechu

# Copia de archivos de carpeta local a apitechu
ADD . /apitechu

# Instalación de dependenciasß
RUN npm install

# Puerto que expone
EXPOSE 3000

# Comando de inicialización
CMD ["npm", "start"]
