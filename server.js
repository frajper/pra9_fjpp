const express = require('express');
const app = express();
const io  = require ('./io');
const userController    = require ('./controllers/UserController');
const authController    = require ('./controllers/AuthController');
const accountController = require ('./controllers/AccountController');
const apunteController  = require ('./controllers/ApunteController');


var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin" , "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000;
app.listen(port);

console.log("API escuchando en el puerto " + port);

// Genarando la cookie de sessión
var session = require('express-session');

// Generate a v4 (random) UUID
var uuid = require('node-uuid');
var secretKey = uuid.v4();
app.use(session({
  secret: secretKey,
  resave: true,
  saveUninitialized: true
}))

app.delete('/apitechu/v1/users/:id' , userController.deleteUserV1);
app.get   ('/apitechu/v1/users'     , userController.getUsersV1);
app.get   ('/apitechu/v2/users'     , userController.getUsersV2);
app.get   ('/apitechu/v2/users/:id' , userController.getUserByIdV2);
app.post  ('/apitechu/v1/users'     , userController.createUserV1);
app.post  ('/apitechu/v2/users'     , userController.createUserV2);
app.put   ('/apitechu/v2/users'     , userController.updateUser);


app.post  ('/apitechu/v1/login'     , authController.loginUsersV1);
app.post  ('/apitechu/v1/logout/:id', authController.logoutUsersV1);
app.post  ('/apitechu/v2/login'     , authController.loginUsersV2);
app.post  ('/apitechu/v2/logout/:id', authController.logoutUsersV2);

app.get   ('/apitechu/v2/accounts/:user_id', accountController.getAccountsByUserIdV2);
app.post  ('/apitechu/v2/accounts/'        , accountController.createAccountV2);
app.put   ('/apitechu/v2/accounts/'        , accountController.updateAccountV2);

app.get   ('/apitechu/v2/apuntes/:iban'    , apunteController.getApuntesByIban);
app.post  ('/apitechu/v2/apuntes/'         , apunteController.createApunte);

app.get('/apitechu/v1/hello',
    function(req, res) {
        console.log('GET /apitechu/v1/hello');
        res.send ({"msg" : "Hola desde API TechU"});
    }
)

app.post('/apitechu/v1/monstruo/:p1/:p2',
    function (req, res) {
        console.log("Query string");
        console.log(req.query);
        console.log("Parámetros");
        console.log(req.params);
        console.log("Headers");
        console.log(req.headers);
        console.log("Body");
        console.log(req.body);

    }
)
