const requestJson = require ('request-json');
const io = require ('../io');
const crypt = require ('../crypt');


const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpp/collections/";
const mLabAPIKey  = "&apiKey=w5dL9AMi45CpeJCc5nCPMZ1IW4txG5iM";

function loginUsersV1(req, res) {

    console.log('POST /apitechu/v1/login');
    console.log("QUERY >>" + JSON.stringify(req.query));

    //res.sendFile ("usuarios.json", {root: __dirname});

    // Cargamos el array de users
    var users = require('../usuarios.json');

    var response = {"mensaje" : "Login incorrecto"};
    var userEmail    = req.body.email;
    var userPassword = req.body.password;

    var i = 0;
    var userFound = false;
    users.forEach(function buscaElementoArray(elemento, index, array){
            if (elemento.email == userEmail && elemento.password == userPassword){
              console.log('>>> USER LOGGED IN');
              userFound = true;
            } else {
              if (!userFound) i++;
            }
          }
    );

    if (userFound){
      users[i].logged = true;
      io.writeUserDataToFile(users);
      response = { "mensaje" : "Login correcto", "idUsuario" : users[i].id };
    } else {
        console.log('No se encontró al usuario ' + userEmail);
    }

    res.send (response);
}

module.exports.loginUsersV1 = loginUsersV1;

function loginUsersV2(req, res) {
  console.log('POST /apitechu/v2/login');

  var response = {"mensaje" : "Login incorrecto"};

  var email = req.body.email;
  var userPassword = req.body.password;

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log('Mlab Client created');

  // Comprobamos si el usuario existe
  console.log("Buscando usuario " + email);
  var query = 'q={"email":"' + email + '"}';

  httpClient.get("user?"+ query + mLabAPIKey,
    function(err, resMLab, body){
      if (err) {
        console.log(err);
        console.log(resMLab);
        console.log("loginUsersV2: Error obteniendo usuarios");
      } else if (body.length > 0){
        console.log("Usuario encontrado");

        // 401 unauthorized
        if (!crypt.checkPassword(userPassword, body[0].password)){
          response = {"mensaje" : "Contraseña IN-correcta de " + email};
          //console.log(crypt.hash(userPassword));
          res.status(401).send(response);
        } else {
          console.log("Contraseña CORRECTA del usuario " + email + " :)");

          // // Genarando la cookie de sessión
          // var cookieSession = require('cookie-session');
          // var express = require('express');
          // var session = require('express-session');
          //
          // var app = express()
          //
          // // Generate a v4 (random) UUID
          // var uuid = require('node-uuid');
          // var secretKey = uuid.v4();
          // app.use(session({
          //   secret: secretKey,
          //   resave: true,
          //   saveUninitialized: true
          // }))
          //
          // req.session.id = email;
          //
          // // Generate a v4 (random) UUID
          // var uuid = require('node-uuid');
          // var secretKey = uuid.v4();
          //
          // app.use(cookieSession({
          //   name: 'session',
          //   keys: [ secretKey ],
          //   // Cookie Options
          //   maxAge: 24 * 60 * 60 * 1000 // 24 hours
          // }))

          req.session.email = email;

          // PARA PRUEBAS PASS $2b$10$1cVgEx31.DJDcUxbCeY3Yu99XJqBYv28UWMN2a8ZLh9Yuo6DROqL.
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?"
            + query
            + "&"
            + mLabAPIKey, JSON.parse(putBody), function(errPut, resMLabPut, bodyPut){
              if (err) {
                console.log("loginUsersV2: Error ACTUALIZANDO usuario: login");
              } else {
                console.log(JSON.stringify(resMLabPut));
                response = {
                  "mensaje" : "Login correcto de " + email,
                  "id" : body[0].id
                };
              }

              res.send(response);
            });
          }
      } else {
        console.log("[!!!] Usuario no encontrado " + email);
        response = {"mensaje" : "Usuario no encontrado"};
        res.status(404).send(response);
      }
    }
  );
}
module.exports.loginUsersV2 = loginUsersV2;

function logoutUsersV1(req, res) {

    console.log('POST /apitechu/v1/logout');
    console.log("QUERY >>" + JSON.stringify(req.query));

    //res.sendFile ("usuarios.json", {root: __dirname});

    // Cargamos el json con el array de users
    var users = require('../usuarios.json');

    var response = {"mensaje" : "Login incorrecto"};
    var userId = req.params.id;

    var i = 0;
    var userFound = false;

    for (var elemento of users){
      if (elemento.id == userId){
        console.log('>>> USER LOGGED OUT');
        userFound = true;
        break;
      } else {
        i++;
      }
    }

    if (userFound){
      delete users[i].logged; // Eliminamos el campo
      io.writeUserDataToFile(users);
      response = { "mensaje" : "Logout correcto", "idUsuario" : users[i].id };
    } else {
        console.log('No se encontró al usuario ' + userId);
    }

    res.send (response);
}

module.exports.logoutUsersV1 = logoutUsersV1;

function logoutUsersV2(req, res) {
  console.log('POST /apitechu/v2/logout');

  var response = {"mensaje" : "Login incorrecto"};

  var id = req.params.id;

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log('Mlab Client created');

  // Comprobamos si el usuario existe
  console.log("Buscando usuario con ID " + id);
  var query = 'q={"id":' + id + '}';

  httpClient.get("user?"+ query + mLabAPIKey,
    function(err, resMLab, body){
      if (err) {
        console.log("logoutUsersV2: Error obteniendo usuario " + query);
      } else if (body.length > 0){
        console.log("Usuario encontrado");

        var user = body[0];
        // 400 unauthorized // El usuario no ha iniciado sesión
        if (! user.logged){
          response = {"mensaje" : "Usuario " + id + " sin sesión iniciada"};
          res.status(400).send(response);
        } else {
          console.log("Usuario con sesión iniciada " + id + " :)");
          var putBody = '{"$unset":{"logged":""}}';

          httpClient.put("user?"
            + query
            + "&"
            + mLabAPIKey, JSON.parse(putBody), function(errPut, resMLabPut, bodyPut){
              if (err) {
                console.log("logoutUsersV2: Error ACTUALIZANDO usuario: login");
              } else {
                console.log(JSON.stringify(resMLabPut));
                response = {"mensaje" : "LogOUT correcto de " + id + " - mail " + user.email};
              }

              res.send(response);
            });
          }
      } else {
        console.log("logoutUsersV2: Usuario no encontrado");
        res.status(404).send(response);
      }
    }
  );
}
module.exports.logoutUsersV2 = logoutUsersV2;
