const io = require ('../io');
const requestJson = require ('request-json');
const crypt = require ('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpp/collections/";
const mLabAPIKey = "&apiKey=w5dL9AMi45CpeJCc5nCPMZ1IW4txG5iM";

function getUsersV2(req, res) {
    console.log('GET /apitechu/v2/users');
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Mlab Client created');

    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }

        console.log("Array length: " + body.length);
        res.send(response);
      }
    );
}
module.exports.getUsersV2 = getUsersV2;

function getUserByIdV2(req, res) {
    console.log('GET /apitechu/v2/users/:id');
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Mlab Client created');

    // Prueba datos sesión recibidos
    console.log("Datos sesión se MANTIENEN: " + req.session.email);

    var id = req.params.id;
    console.log("Buscando usuario " + id);
    var query = 'q={"id":' + id + '}';
    httpClient.get("user?"+ query + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            console.log("Usuarios q hacen match: " + body.length);
            var response = body[0];
          } else {
            var response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }
        }
        res.send(response);
      }
    );
}
module.exports.getUserByIdV2 = getUserByIdV2;

function getUserByEmail(email) {
    console.log('getUserByMail');
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Mlab Client created');

    console.log("Buscando usuario " + email);
    var query = 'q={"email":"' + email + '"}';
    httpClient.get("user?"+ query + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          console.log("getUserByEmail: Error obteniendo usuarios");
          return -1;
        }

        if (body.length > 0){
          return body[0];
        }

        console.log("getUserByEmail: Usuario no encontrado");

        var response = {"msg" : "Usuario no encontrado"};
        return -1;
      }
    );
}


function getUsersV1(req, res) {

    console.log('GET /apitechu/v1/users');
    console.log("QUERY >>" + JSON.stringify(req.query));

    //res.sendFile ("usuarios.json", {root: __dirname});

    // Cargamos el array de users
    var arrayUsers = require('../usuarios.json');

    var response = {};

    var count = req.query.$count;
    if (typeof(count) != 'undefined' && count == 'true'){
        console.log('================= COUNT ' + count + '<<');
        response.length = arrayUsers.length;
    }

    if (typeof(req.query.$top) != 'undefined'){
        console.log('================= TOP ' + req.query.$top + '<<');
        var top = req.query.$top;
        var arrayTop = arrayUsers.slice(0, top);
        console.log('TAM SLICED: ' + arrayTop.length);
        response.data = arrayTop;
    } else {
        response.data = arrayUsers;
    }

    res.send (response);
}

module.exports.getUsersV1 = getUsersV1;

function createUserV1(req, res) {
    console.log('POST /apitechu/v1/users' + JSON.stringify(req.headers));

    var headers = req.headers;

/*        var newUser = {
        "first_name": headers.first_name,
        "last_name": headers.last_name,
        "email": headers.email
    }
*/
    var newUser = {
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email
    }

    var users = require('../usuarios.json');
    users.push(newUser);
    console.log("Usuario añadido");


    io.writeUserDataToFile(users);
    //res.send ();
}
module.exports.createUserV1 = createUserV1;

///////////////////////////////////////////////
/// UPDATE >>> PUT
///////////////////////////////////////////////
function updateUser(req, res){
  console.log('PUT /apitechu/v2/user');

  var id = req.body.id;
  var updateUser = {
      "first_name": req.body.first_name,
      "last_name" : req.body.last_name,
      "email"     : req.body.email,
      "password"  : crypt.hash(req.body.password)
  }

  var query = 'q={"id":' + id + '}';

  //var putBody = '{"$set":' + JSON.stringify(updateUser) + '}';
  var putBody = {"$set":updateUser};

  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.put("user?"
    + query
    + "&"
    + mLabAPIKey, putBody, function(errPut, resMLabPut, bodyPut){
      if (errPut) {
        console.log("updateUser: Error ACTUALIZANDO usuario");
      } else {
        console.log(JSON.stringify(resMLabPut));
        response = {"mensaje" : "UPDATE correcto de " + id + " - mail " + updateUser.email};
      }

      res.send(response);
    });
}
module.exports.updateUser = updateUser;

function createUserV2(req, res) {
    //console.log('POST /apitechu/v2/users' + JSON.stringify(req.headers));
    console.log('POST /apitechu/v2/users' + JSON.stringify(req.body));

    var httpClient = requestJson.createClient("http://localhost:3000/apitechu/v2/");
    console.log('Apitechu Client created');

    httpClient.get("users",
      function(err, resMLab, body){

        if (!err){
          var newID = body.length + 1;
          console.log("ID del nuevo usuario: " + newID);

          var newUser = {
              "id" : newID,
              "first_name": req.body.first_name,
              "last_name": req.body.last_name,
              "email": req.body.email,
              "gender": req.body.gender,
              "password": crypt.hash(req.body.password)
          }

          var httpClient = requestJson.createClient(baseMlabURL);
          httpClient.post("user?" + mLabAPIKey, newUser,
            function(err, resMLab, body){
              console.log("Usuario guardado con éxito");
              res.status(201).send({"msg" : "Usuario guardado con éxito"})
            }
          );
        }
        else {
          console.log("createUserV2: Error obteniendo usuarios");
          var response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500).send(response);
        }
      }
    );
}
module.exports.createUserV2 = createUserV2;

function deleteUserV1(req, res) {

    console.log('DELETE /apitechu/v1/users/:id');

    var userId = req.params.id;
    console.log("QUERY >>" + JSON.stringify(req.query));
    console.log('usuario a eliminar con ID ' + userId);

    // Cargamos los usuarios
    var users = require("../usuarios.json");
    //var usersNew = [];
    //for (var i in users){
    var i = 0;
    users.forEach(function eliminaElementoArray(elemento, index, array){
            if (elemento.id == userId)
                users.splice(index, 1);
            else console.log(index + ': ' + 'userId ' + users[index].id);
        }
    );
    io.writeUserDataToFile(users);

    /*for (var user of users){
        i++;
        //var user = users[i];
        if (user.id == userId){
            users.splice(i, 1);
            break;
            //usersNew.push(user);
            JSON.stringify(user);
            console.log('> usuario eliminado con ID ' + userId);
            io.writeUserDataToFile(users);
        } else console.log('userId ' + user.id);
    }*/
}

module.exports.deleteUserV1 = deleteUserV1;
