const io = require ('../io');
const requestJson = require ('request-json');
const crypt = require ('../crypt');

const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufjpp/collections/";
const mLabAPIKey = "&apiKey=w5dL9AMi45CpeJCc5nCPMZ1IW4txG5iM";

function getApuntesByIban(req, res) {
    console.log('GET /apitechu/v2/apuntes/:iban');
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('Mlab Client created');

    var iban = req.params.iban;
    console.log("Buscando movimientos cuenta " + iban);
    var query = 'q={"iban": "' + iban + '"}';
    httpClient.get("apunte?"+ query + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          var response = {
            "msg" : "Error obteniendo apuntes"
          }
          res.status(500);
        } else {
          if (body.length > 0){
            console.log("Movimientos q hacen match: " + body.length);
            var response = body;
          } else {
            var response = {"msg" : "Movimientos no encontrados (asociados al iban)"};
            res.status(404);
          }
        }
        res.send(response);
      }
    );
}
module.exports.getApuntesByIban = getApuntesByIban;

function createApunte(req, res) {
    console.log('POST /apitechu/v2/apuntes ' + JSON.stringify(req.body));

    var httpClient = requestJson.createClient("http://localhost:3000/apitechu/v2/");
    console.log('Apitechu Client created');

    httpClient.get("apuntes/" + req.body.iban,
      function(err, resMLab, body){

        if (!err){
          var newID = body.length + 1;
          console.log("ID del nuevo apunte: " + newID);

          var newApunte = {
              "id"         : newID,
              "description": req.body.description,
              "amount"     : req.body.amount,
              "currency"   : req.body.currency,
              "iban"       : req.body.iban,
              "date"       : Date.now(),
              "location"   : req.body.location
          }

          var httpClient = requestJson.createClient(baseMlabURL);
          httpClient.post("apunte?" + mLabAPIKey, newApunte,
            function(err, resMLab, body){
              if (!err) {
                console.log("Apunte guardado con éxito");
                res.status(201).send({"msg" : "Apunte guardado con éxito"})
              } else {
                console.log("createApunte: Error creando apunte");
                var response = {
                  "msg" : "Error creando apunte"
                }
                res.status(500).send(response);
              }
            }
          );
        }
        else {
          console.log("createApunte: Error obteniendo apuntes");
          var response = {
            "msg" : "Error obteniendo apunte"
          }
          res.status(500).send(response);
        }
      }
    );
}
module.exports.createApunte = createApunte;

function deleteUserV1(req, res) {

    console.log('DELETE /apitechu/v1/users/:id');

    var userId = req.params.id;
    console.log("QUERY >>" + JSON.stringify(req.query));
    console.log('usuario a eliminar con ID ' + userId);

    // Cargamos los usuarios
    var users = require("../usuarios.json");
    //var usersNew = [];
    //for (var i in users){
    var i = 0;
    users.forEach(function eliminaElementoArray(elemento, index, array){
            if (elemento.id == userId)
                users.splice(index, 1);
            else console.log(index + ': ' + 'userId ' + users[index].id);
        }
    );
    io.writeUserDataToFile(users);

    /*for (var user of users){
        i++;
        //var user = users[i];
        if (user.id == userId){
            users.splice(i, 1);
            break;
            //usersNew.push(user);
            JSON.stringify(user);
            console.log('> usuario eliminado con ID ' + userId);
            io.writeUserDataToFile(users);
        } else console.log('userId ' + user.id);
    }*/
}

module.exports.deleteUserV1 = deleteUserV1;
