const io = require ('../io');
const requestJson = require ('request-json');
const crypt = require ('../crypt');

const baseMlabURL  = "https://api.mlab.com/api/1/databases/apitechufjpp/collections/";
const mLabAPIKey   = "&apiKey=w5dL9AMi45CpeJCc5nCPMZ1IW4txG5iM";
const baseFixerURL = "http://data.fixer.io/api/latest";
const fixerAPIKey  = "e9c87cfe8113e426cdbc164508393743";


/*function getAccountsV2(req, res) {
    console.log('GET /apitechu/v2/accounts');
    var httpClient = requestJson.createClient(baseMlabURL);
    console.log('MLAB Client created');

    httpClient.get("account?" + mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }
    );
}
module.exports.getAccountsV2 = getAccountsV2;*/

function getAccountsByUserIdV2(req, res) {
    console.log('GET /apitechu/v2/account/:user_id');
    var httpClient = requestJson.createClient(baseMlabURL);
    var httpClient2 = requestJson.createClient(baseFixerURL);
    console.log('Client created');

    var user_id = req.params.user_id;
    console.log("Buscando accounts del usuario " + user_id);
    var query = 'q={"user_id":' + user_id + '}';
    httpClient.get("account?"+ query + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuarios"
          }
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0){
            console.log("Accounts q hacen match: " + body.length);
            var response = body;

            // Conversión de EUR a DÓLAR
            var exchangeRate;
            httpClient2.get("?access_key=" + fixerAPIKey + "&symbols=USD",
              function(errFixer, resFixer, bodyFixer){
                if (errFixer) {
                  console.log("Error al llamar a FIXER");
                  var responseFixer = {
                    "msg" : "Error al llamar a FIXER"
                  }
                  res.status(500);
                  res.send(response);
                } else {
                  if (JSON.stringify(bodyFixer).length > 0){
                    exchangeRate = bodyFixer.rates.USD;
                    console.log(bodyFixer);
                    console.log("CAMBIO EUR-DOLAR 1 >>> " + exchangeRate);

                    // CONVERSIONES A DÓLARES DE CADA CUENTA AQUÍ
                    for (var i in response){
                      var balanceUSD = response[i].balance * exchangeRate;
                      response[i].balanceUSD = (balanceUSD !== 0) ? balanceUSD.toFixed(2) : 0;
                      console.log("EUR >> " + response[i].balance + " USD >> " + response[i].balanceUSD);
                    }
                    res.send(response);

                  } else {
                    console.log("Falló la llamada a FIXER");
                    var responseFixer = {"msg" : "Falló la llamada a FIXER"};
                  }
                }
            });

          } else {
            var response = {"msg" : "Account NO encontrada"};
            res.status(404);
            res.send(response);
          }
        }
      }
    );
}
module.exports.getAccountsByUserIdV2 = getAccountsByUserIdV2;

function createAccountV2(req, res) {
    //console.log('POST /apitechu/v2/accounts' + JSON.stringify(req.headers));
    console.log('POST /apitechu/v2/accounts' + JSON.stringify(req.body));

    var user_id = req.body.user_id;

    var ibanGenerator = require("iban-generator")
    var str_user_id = user_id + "";
    var length = str_user_id.length;
    // Adding 0s to the left side to get a 20 digit account IBAN
    for (var i = length; i < 4; i++)
      str_user_id = "0" + str_user_id;

    var ccc = "1234567890612345" + str_user_id;
    var iban = ibanGenerator.doIban(ccc);
    console.log("IBAN >>> " + iban);

    var newAccount = {
        "iban": iban,
        "user_id" : user_id,
        "balance": 0
    }

    var httpClient = requestJson.createClient(baseMlabURL);
    httpClient.post("account?" + mLabAPIKey, newAccount,
      function(err, resMLab, body){
        console.log("IBAN del usuario " + user_id + " guardado con éxito");
        res.status(201).send({"msg" : "Account guardada con éxito"})
      }
    );
}
module.exports.createAccountV2 = createAccountV2;

function updateAccountV2(req, res) {
    //console.log('POST /apitechu/v2/users' + JSON.stringify(req.headers));
    console.log('PUT /apitechu/v2/accounts' + JSON.stringify(req.body));

    var user_id = req.body.user_id;

    console.log('PUT /apitechu/v2/user');

    var iban = req.body.iban;
    var updateAccount = {
          "iban": iban,
          "user_id" : user_id,
          "balance": balance
      }


    var query = 'q={"iban":' + iban + '}';

    //var putBody = '{"$set":' + JSON.stringify(updateUser) + '}';
    var putBody = {"$set":updateAccount};

    var httpClient = requestJson.createClient(baseMlabURL);
    httpClient.put("account?"
      + query
      + "&"
      + mLabAPIKey, putBody, function(errPut, resMLabPut, bodyPut){
        if (errPut) {
          console.log("updateUser: Error ACTUALIZANDO account");
        } else {
          console.log(JSON.stringify(resMLabPut));
          response = {"mensaje" : "UPDATE correcto de account " + iban + " - " + updateUser.balance};
        }

        res.send(response);
      });
}
module.exports.updateAccountV2 = updateAccountV2;
